package niaz.testkazan2;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import niaz.testkazan2.model.data.MyDatabase;

import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)

public class ExampleInstrumentedTest {
    private Context context;

    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context context = InstrumentationRegistry.getTargetContext();
        this.context = context;

        assertEquals("niaz.testkazan2", context.getPackageName());

        testDatabase();
    }

    public void testDatabase(){
        MyDatabase myDatabase = new MyDatabase(context);
        int housesSize = 0;
        try {
            myDatabase.delete();
            myDatabase.createOrOpen();
            myDatabase.createHouses();
            housesSize = myDatabase.getHouses().size();
        }
        catch (IOException e){
            Log.d(MainActivity.TAG, "Error to create database");
        }

        assertEquals(myDatabase.exists(), true);
        assertEquals(housesSize, 3);
    }
}
