package niaz.testkazan2.model.flats;

import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.util.List;

import niaz.testkazan2.model.data.Flat;
import niaz.testkazan2.model.data.MyDatabase;
import niaz.testkazan2.MainActivity;

/**
 * Created by NS on 05.06.2017.
 */

public class FlatsModel implements IFlatsModel {
    MyDatabase myDatabase;

    public FlatsModel(Context context){
        myDatabase = new MyDatabase(context);
    }

    /**
     * Get houses
     * @return
     */
    public List<Flat> getFlatsFromDatabase(String houseId){
        Log.d(MainActivity.TAG, "getFlatsFromDatabase");
        try {
            Thread.sleep(3000);
        }
        catch (InterruptedException e){
            Log.e(MainActivity.TAG, "Error to do delay");
        }

        try {
            myDatabase.createOrOpen();
            List<Flat> flats = myDatabase.getFlats(houseId);
            myDatabase.close();
            return flats;
        }
        catch (IOException e){
            return null;
        }
    }

}
