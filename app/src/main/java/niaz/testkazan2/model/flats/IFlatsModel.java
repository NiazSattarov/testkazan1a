package niaz.testkazan2.model.flats;

import java.util.List;

import niaz.testkazan2.model.data.Flat;

/**
 * Created by NS on 05.06.2017.
 */

public interface IFlatsModel {
    public List<Flat> getFlatsFromDatabase(String houseId);
}
