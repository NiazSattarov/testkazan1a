package niaz.testkazan2.model.login;

import android.content.Context;
import android.util.Log;

import java.io.IOException;

import niaz.testkazan2.common.MyConstants;
import niaz.testkazan2.model.data.MyDatabase;
import niaz.testkazan2.MainActivity;

/**
 * Created by NS on 05.06.2017.
 */

public class LoginModel implements ILoginModel {

    MyDatabase myDatabase;
//    Context context;

    public LoginModel(Context context){
        myDatabase = new MyDatabase(context);
    }

    /**
     * Check login
     * @param login
     * @param password
     * @return
     */
    @Override
    public int checkLogin(String login, String password){
        try {
            Thread.sleep(3000);
        }
        catch (InterruptedException e){
            Log.e(MainActivity.TAG, "Error to do delay");
        }

        try {
            myDatabase.createOrOpen();
            boolean result = myDatabase.checkLoginAndPassword(login, password);
            myDatabase.close();
            if (result){
                return MyConstants.LOGIN_OK;
            }
            else {
                return MyConstants.LOGIN_INVALID;
            }
        }
        catch (IOException e){
            return MyConstants.LOGIN_CONNECTION_ERROR;
        }

    }

}
