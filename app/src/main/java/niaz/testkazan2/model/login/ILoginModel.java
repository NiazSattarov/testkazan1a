package niaz.testkazan2.model.login;

/**
 * Created by NS on 05.06.2017.
 */

public interface ILoginModel {
    public int checkLogin(String login, String password);
}
