package niaz.testkazan2.model.houses;

import java.util.List;

import niaz.testkazan2.model.data.House;

/**
 * Created by NS on 05.06.2017.
 */

public interface IHousesModel {
    public List<House> getHousesFromDatabase();
    public List<House> updateHousesInDatabase(List<House> houses, List<String> deletedHousesIds);
    public String getLastHouseIdFromDatabase();
}
