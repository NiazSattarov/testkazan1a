package niaz.testkazan2.model.houses;

import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.util.List;

import niaz.testkazan2.model.data.House;
import niaz.testkazan2.model.data.MyDatabase;
import niaz.testkazan2.MainActivity;

/**
 * Created by NS on 05.06.2017.
 */

public class HousesModel implements IHousesModel {
    MyDatabase myDatabase;

    public HousesModel(Context context){
        myDatabase = new MyDatabase(context);
    }


    /**
     * Get houses
     * @return
     */
    public List<House> getHousesFromDatabase(){
        Log.d(MainActivity.TAG, "getHousesFromDatabase");
        try {
            Thread.sleep(3000);
        }
        catch (InterruptedException e){
            Log.e(MainActivity.TAG, "Error to do delay");
        }

        try {
            myDatabase.createOrOpen();
            List<House> houses = myDatabase.getHouses();
            myDatabase.close();
            return houses;
        }
        catch (IOException e){
            return null;
        }
    }


    /**
     * Update houses
     * @return
     */
    public List<House> updateHousesInDatabase(List<House> houses, List<String> deletedHousesIds){
        try {
            Thread.sleep(3000);
        }
        catch (InterruptedException e){
            Log.e(MainActivity.TAG, "Error to do delay");
        }


        try {
            myDatabase.createOrOpen();
            List<House> result = myDatabase.updateHouses(houses, deletedHousesIds);
            myDatabase.close();
            return result;
        }
        catch (IOException e){
            return null;
        }
    }

    @Override
    public String getLastHouseIdFromDatabase(){
        try {
            Thread.sleep(3000);
        }
        catch (InterruptedException e){
            Log.e(MainActivity.TAG, "Error to do delay");
        }


        try {
            myDatabase.createOrOpen();
            String result = myDatabase.getHouseIdNext();
            myDatabase.close();
            return result;
        }
        catch (IOException e){
            return null;
        }
    }


}
