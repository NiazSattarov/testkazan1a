package niaz.testkazan2.view.flats;

import java.util.List;

import niaz.testkazan2.model.data.Flat;
import niaz.testkazan2.model.data.House;

/**
 * Created by NS on 05.06.2017.
 */

public interface IFlatsView {
    public void onGetFlatsResult(List<Flat> flats);
}
