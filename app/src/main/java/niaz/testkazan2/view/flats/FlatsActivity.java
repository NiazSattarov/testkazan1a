package niaz.testkazan2.view.flats;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import niaz.testkazan2.R;
import niaz.testkazan2.presenter.flats.FlatsPresenter;
import niaz.testkazan2.presenter.flats.IFlatsPresenter;
import niaz.testkazan2.MainActivity;
import niaz.testkazan2.view.adapters.FlatsAdapter;
import niaz.testkazan2.model.data.Flat;
import niaz.testkazan2.model.data.MyDatabase;

/**
 * Created by NS on 03.06.2017.
 */
public class FlatsActivity extends AppCompatActivity implements IFlatsView{
    private IFlatsPresenter iFlatsPresenter;

    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    private EditText editTextDescription;

    private FlatsAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private List<Flat> flats;
    private MyDatabase myDatabase;
    private String houseId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flats);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        Intent intent = getIntent();
        houseId = intent.getStringExtra("houseId");
        Log.d(MainActivity.TAG, "FlatsActivity houseId=" + houseId);

        init();
        initViews();

        progressBar.setVisibility(View.VISIBLE);
        iFlatsPresenter.getFlatsRequest(houseId);
    }

    private void init(){
        iFlatsPresenter = new FlatsPresenter(this);
    }

    /**
     * Init views
     */
    private void initViews(){
        progressBar = (ProgressBar) findViewById(R.id.flats_progress_bar);
        progressBar.setVisibility(View.VISIBLE);

        editTextDescription = (EditText) findViewById(R.id.flat_description_new);

        findViewById(R.id.flat_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String description = editTextDescription.getText().toString();
                if (description.trim().isEmpty()) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_data_not_defined), Toast.LENGTH_LONG).show();
                    return;
                }

                editTextDescription.setText("");
            }
        });

        findViewById(R.id.button_flats_save_changes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.flats_recycler_view);
    }

    /**
     * Init adapter
     */
    private void initAdapter(){
        adapter = new FlatsAdapter(FlatsActivity.this, flats);
        recyclerView.setAdapter(adapter);
        layoutManager = new GridLayoutManager(getApplicationContext(), 1);
        recyclerView.setLayoutManager(layoutManager);
    }

//    /**
//     * Get flats request
//     */
//    private void getFlatsRequest(){
//        progressBar.setVisibility(View.VISIBLE);
//        Observable.fromCallable(() -> {
//            return getFlatsFromDatabase();
//        })
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribeOn(Schedulers.io())
//                .subscribe(
//                        flats -> {
//                            progressBar.setVisibility(View.INVISIBLE);
//                            Log.d(MainActivity.TAG, "loaded flats=" + flats.size());
//                            this.flats = flats;
//                            if (flats == null){
//                                Log.e(MainActivity.TAG, getResources().getString(R.string.error_database));
//                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_database), Toast.LENGTH_LONG).show();
//                                return;
//                            }
//                            initAdapter();
//                        }
//                );
//    }

    @Override
    public void onGetFlatsResult(List<Flat> flats){
        progressBar.setVisibility(View.INVISIBLE);
        Log.d(MainActivity.TAG, "loaded flats=" + flats.size());
        this.flats = flats;
        if (flats == null){
            Log.e(MainActivity.TAG, getResources().getString(R.string.error_database));
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_database), Toast.LENGTH_LONG).show();
            return;
        }
        initAdapter();
    }

}