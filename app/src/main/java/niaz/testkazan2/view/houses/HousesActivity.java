package niaz.testkazan2.view.houses;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import niaz.testkazan2.R;
import niaz.testkazan2.presenter.houses.HousesPresenter;
import niaz.testkazan2.presenter.houses.IHousesPresenter;
import niaz.testkazan2.MainActivity;
import niaz.testkazan2.view.adapters.HousesAdapter;
import niaz.testkazan2.model.data.House;
import niaz.testkazan2.model.data.MyDatabase;

/**
 * Created by NS on 01.06.2017.
 */
public class HousesActivity extends AppCompatActivity implements IHousesView{
    private IHousesPresenter iHousesPresenter;

    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    private EditText editTextDescription;

    private HousesAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private List<House> houses;
    private MyDatabase myDatabase;
    private boolean dataChanged = false;
    private List<String> deletedHousesIds = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_houses);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        init();
        initViews();

        progressBar.setVisibility(View.VISIBLE);
        iHousesPresenter.getHousesRequest();
    }

    private void init(){
        iHousesPresenter = new HousesPresenter(this);
    }

    /**
     * We have to clear update parameters after return to activity
     */
    @Override
    protected void onResume(){
        super.onResume();
        dataChanged = false;
        deletedHousesIds.clear();
    }

    /**
     * Init views
     */
    private void initViews(){
        progressBar = (ProgressBar) findViewById(R.id.houses_progress_bar);
        progressBar.setVisibility(View.VISIBLE);

        editTextDescription = (EditText) findViewById(R.id.house_description_new);

        findViewById(R.id.house_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String description = editTextDescription.getText().toString();
                if (description.trim().isEmpty()) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_data_not_defined), Toast.LENGTH_LONG).show();
                    return;
                }
                progressBar.setVisibility(View.VISIBLE);
                iHousesPresenter.getLastHouseId();
            }
        });

        findViewById(R.id.button_houses_save_changes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (progressBar.getVisibility() == View.VISIBLE){
                    return;
                }
                if (dataChanged) {
                    progressBar.setVisibility(View.VISIBLE);
                    iHousesPresenter.updateHousesRequest(houses, deletedHousesIds);
                }
                else {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.message_not_changed), Toast.LENGTH_LONG).show();
                }
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.houses_recycler_view);
    }

    /**
     * Add houseId to delete list
     * @param deletedHouseId
     */
    public void addDeletedHouseId(String deletedHouseId){
        deletedHousesIds.add(deletedHouseId);
    }

    /**
     * Init adapter
     */
    public void initAdapter(){
        if (houses != null) {
            adapter = new HousesAdapter(this, houses);
            recyclerView.setAdapter(adapter);
            layoutManager = new GridLayoutManager(getApplicationContext(), 1);
            recyclerView.setLayoutManager(layoutManager);
        }
    }

    /**
     * Set parameter which says that data is changed
     */
    public void setDataChanged(){
        dataChanged = true;
    }

    /**
     * Getter for dataChanged
     * @return
     */
    public boolean isDataChanged(){
        return dataChanged;
    }


    @Override
    public void onGetHousesResult(List<House> houses){
        progressBar.setVisibility(View.INVISIBLE);
        Log.d(MainActivity.TAG, "returned houses=" + houses.size());
        this.houses = houses;
        dataChanged = false;
        if (houses == null){
            Log.e(MainActivity.TAG, getResources().getString(R.string.error_database));
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_database), Toast.LENGTH_LONG).show();
            return;
        }
        initAdapter();
    }

    @Override
    public void onUpdateHousesResult(List<House> houses){
        progressBar.setVisibility(View.INVISIBLE);
        Log.d(MainActivity.TAG, "returned houses=" + houses.size());
        this.houses = houses;
        dataChanged = false;
        if (houses == null){
            Log.e(MainActivity.TAG, getResources().getString(R.string.error_database));
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_database), Toast.LENGTH_LONG).show();
            return;
        }
        Toast.makeText(getApplicationContext(), getResources().getString(R.string.message_update), Toast.LENGTH_LONG).show();
//        initAdapter();
    }

    @Override
    public void onGetLastHouseId(String houseId){
        progressBar.setVisibility(View.INVISIBLE);
        House house = new House(houseId, editTextDescription.getText().toString());
        house.setNewItem(true);
        houses.add(house);
        dataChanged = true;
        initAdapter();
        editTextDescription.setText("");
    }

}