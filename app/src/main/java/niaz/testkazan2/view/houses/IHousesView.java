package niaz.testkazan2.view.houses;

import java.util.List;

import niaz.testkazan2.model.data.House;

/**
 * Created by NS on 05.06.2017.
 */

public interface IHousesView {
    public void onGetHousesResult(List<House> houses);
    public void onUpdateHousesResult(List<House> houses);
    public void onGetLastHouseId(String houseId);
}
