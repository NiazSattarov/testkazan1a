package niaz.testkazan2.view.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;
import niaz.testkazan2.R;
import niaz.testkazan2.view.flats.FlatsActivity;
import niaz.testkazan2.view.houses.HousesActivity;
import niaz.testkazan2.MainActivity;
import niaz.testkazan2.model.data.House;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

/**
 * Recycler view adapter
 */
public class HousesAdapter extends RecyclerView.Adapter<HousesAdapter.ViewHolder>// implements DataRequest
{
    private Context context;
    private List<House> houses;
    private HousesActivity housesActivity;

    /**
     * Constructor
     */
    public HousesAdapter(HousesActivity housesActivity, List<House> houses){
        this.housesActivity = housesActivity;
        context = housesActivity.getApplicationContext();
        this.houses = houses;
    }

    /**
     * View Holder
     */
    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        public EditText editTextDescription;
        public Button buttonDelete;
        public Button buttonFlats;

        public ViewHolder(View v){
            super(v);
            editTextDescription = (EditText) v.findViewById(R.id.house_description);
            buttonDelete = (Button) v.findViewById(R.id.house_delete);
            buttonFlats = (Button) v.findViewById(R.id.house_flats);
        }
    }

    /**
     * On create holder
     * @param parent
     * @param viewType
     * @return
     */
    @Override
    public HousesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        // Create a new View
        View v = LayoutInflater.from(context).inflate(R.layout.house_item, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    /**
     * Bind view holder
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position){
        holder.editTextDescription.setText(houses.get(holder.getAdapterPosition()).getHouseDescription());
        holder.editTextDescription.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {}

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.d(MainActivity.TAG, "changed pos=" + position + " str=" + s.toString());
                House house = houses.get(position);
                house.setHouseDescription(s.toString());
                house.setChanged(true);
                housesActivity.setDataChanged();
            }
        });

        holder.buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(MainActivity.TAG, "delete pos=" + position);

                housesActivity.addDeletedHouseId(houses.get(position).getHouseId());
                housesActivity.setDataChanged();
                houses.remove(position);
                housesActivity.initAdapter();
            }
        });

        holder.buttonFlats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (housesActivity.isDataChanged()){
                    Toast.makeText(context.getApplicationContext(), context.getResources().getString(R.string.message_need_to_save), Toast.LENGTH_LONG).show();
                    return;
                }

                Log.d(MainActivity.TAG, "flats pos=" + position);
                Intent intent = new Intent(context, FlatsActivity.class);
                intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("houseId", houses.get(position).getHouseId());
                context.startActivity(intent);

            }
        });
    }

    /**
     * Get count
     * @return
     */
    @Override
    public int getItemCount(){
        return houses.size();
    }

    public List<House> getHouses(){
        return houses;
    }

}