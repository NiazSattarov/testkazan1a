package niaz.testkazan2;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import niaz.testkazan2.model.data.MyDatabase;
import niaz.testkazan2.view.login.LoginActivity;

/**
 * Main activity for app is just to run main code
 */
public class MainActivity extends AppCompatActivity {
    public static final String TAG = "myApp";
    MyDatabase myDatabase;
    private ProgressBar progressBar;

    /**
     * On create
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        initViews();
    }

    /**
     * Init views
     */
    private void initViews(){
        progressBar = (ProgressBar) findViewById(R.id.main_progress_bar);

        findViewById(R.id.my_button_create).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (progressBar.getVisibility() == View.VISIBLE){
                    return;
                }

                myDatabase = new MyDatabase(getApplicationContext());
                createDatabaseAsync();
            }
        });

        findViewById(R.id.my_button_exit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    /**
     * Create database
     */
    private void createDatabaseAsync(){
        progressBar.setVisibility(View.VISIBLE);
        Observable.fromCallable(() -> {
            return createDatabase();
        })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        result -> {
                            progressBar.setVisibility(View.INVISIBLE);
                            Log.d(MainActivity.TAG, "result=" + result);
                            if (!myDatabase.exists()){
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_database_not_created), Toast.LENGTH_LONG).show();
                                return;
                            }

                            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                            startActivity(intent);
                        }
                );
    }


    /**
     * Create database
     * @return
     */
    private boolean createDatabase(){
        // Create database
        Log.d(TAG, "Create database");

        try {
            myDatabase.delete();
            myDatabase.createOrOpen();

            if (!myDatabase.isOpen()){
                showDatabaseError();
                return false;
            }

            myDatabase.createUsers();
            myDatabase.createHouses();
            myDatabase.createFlats();

            myDatabase.printItemsTable();
            myDatabase.printUsersTable();
            myDatabase.close();
        }
        catch (Exception e){
            showDatabaseError();
            return false;
        }
        if (!myDatabase.exists()){
            return false;
        }

        return true;

    }

    /**
     * Show database error
     */
    private void showDatabaseError(){
        Log.e(TAG, getResources().getString(R.string.error_database));
        Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_database), Toast.LENGTH_LONG).show();
    }

    /**
     * Disable BACK key
     */
    @Override
    public void onBackPressed() {
    }
}
