package niaz.testkazan2.presenter.flats;

/**
 * Created by NS on 05.06.2017.
 */

public interface IFlatsPresenter {
    public void getFlatsRequest(String houseId);
}
