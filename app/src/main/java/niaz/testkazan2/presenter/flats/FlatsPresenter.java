package niaz.testkazan2.presenter.flats;

import android.content.Context;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import niaz.testkazan2.model.flats.FlatsModel;
import niaz.testkazan2.presenter.flats.IFlatsPresenter;
import niaz.testkazan2.view.flats.FlatsActivity;
import niaz.testkazan2.view.flats.IFlatsView;

/**
 * Created by NS on 05.06.2017.
 */

public class FlatsPresenter implements IFlatsPresenter {
    IFlatsView iFlatsView;
    Context context;
    FlatsModel flatsModel;

    /**
     * Constructor
     * @param iFlatsView
     */
    public FlatsPresenter(IFlatsView iFlatsView){
        this.iFlatsView = iFlatsView;
        context = (FlatsActivity)iFlatsView;
        flatsModel = new FlatsModel(context);
    }

    /**
     * Get flats request
     */
    @Override
    public void getFlatsRequest(String houseId){
        Observable.fromCallable(() -> {
            return flatsModel.getFlatsFromDatabase(houseId);
        })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        flats -> {
                            iFlatsView.onGetFlatsResult(flats);
                        }
                );
    }

}
