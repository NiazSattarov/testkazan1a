package niaz.testkazan2.presenter.login;

import android.content.Context;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import niaz.testkazan2.model.login.LoginModel;
import niaz.testkazan2.view.login.ILoginView;
import niaz.testkazan2.view.login.LoginActivity;

/**
 * Login presenter
 * Created by NS on 05.06.2017.
 */
public class LoginPresenter implements ILoginPresenter{

    ILoginView iLoginView;
    LoginModel loginModel;
    Context context;

    /**
     * Constructor
     * @param iLoginView
     */
    public LoginPresenter(ILoginView iLoginView){
        this.iLoginView = iLoginView;
        context = (LoginActivity)iLoginView;
        loginModel = new LoginModel(context);
    }

    /**
     * Do login
     * @param login
     * @param password
     */
    @Override
    public void doLogin(String login, String password){
        Observable.fromCallable(() -> {
            return loginModel.checkLogin(login, password);
        })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        result -> {
                            iLoginView.onLoginResult(result);
                        }
                );

    }

}
