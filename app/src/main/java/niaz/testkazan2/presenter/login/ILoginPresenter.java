package niaz.testkazan2.presenter.login;

/**
 * Created by NS on 05.06.2017.
 */

public interface ILoginPresenter {
    void doLogin(String name, String passwd);
}
