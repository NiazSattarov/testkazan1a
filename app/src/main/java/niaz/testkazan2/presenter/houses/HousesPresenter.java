package niaz.testkazan2.presenter.houses;

import android.content.Context;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import niaz.testkazan2.model.houses.HousesModel;
import niaz.testkazan2.model.data.House;
import niaz.testkazan2.view.houses.HousesActivity;
import niaz.testkazan2.view.houses.IHousesView;

/**
 * Houses presenter
 * Created by NS on 05.06.2017.
 */
public class HousesPresenter implements IHousesPresenter {
    private IHousesView iHousesView;
    private HousesModel housesModel;
    private Context context;

    /**
     * Constructor
     * @param iHousesView
     */
    public HousesPresenter(IHousesView iHousesView){
        this.iHousesView = iHousesView;
        context = (HousesActivity)iHousesView;
        housesModel = new HousesModel(context);
    }

    /**
     * Get houses request
     */
    @Override
    public void getHousesRequest(){
        Observable.fromCallable(() -> {
            return housesModel.getHousesFromDatabase();
        })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        houses -> {
                            iHousesView.onGetHousesResult(houses);
                        }
                );
    }

    /**
     * Update houses request
     * @param housesSrc
     * @param deletedHousesIds
     */
    @Override
    public void updateHousesRequest(List<House> housesSrc, List<String> deletedHousesIds){
        Observable.fromCallable(() -> {
            return housesModel.updateHousesInDatabase(housesSrc, deletedHousesIds);
        })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        houses -> {
                            iHousesView.onUpdateHousesResult(houses);
                        }
                );
    }

    /**
     * Get Last houseID
     */
    @Override
    public void getLastHouseId(){
        Observable.fromCallable(() -> {
            return housesModel.getLastHouseIdFromDatabase();
        })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        result -> {
                            iHousesView.onGetLastHouseId(result);
                        }
                );
    }

}
