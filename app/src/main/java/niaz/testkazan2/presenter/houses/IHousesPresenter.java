package niaz.testkazan2.presenter.houses;

import java.util.List;

import niaz.testkazan2.model.data.House;

/**
 * Created by NS on 05.06.2017.
 */

public interface IHousesPresenter {
    public void getHousesRequest();
    public void updateHousesRequest(List<House> houses, List<String> deletedHousesIds);
    public void getLastHouseId();
}
